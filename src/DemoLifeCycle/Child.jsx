import React, { Component, PureComponent } from "react";

export default class Child extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      message: "Hello Child",
    };
    console.log("constructor child");
  }

  shouldComponentUpdate(nextProps) {
    return true;
  }
  static getDerivedStateFromProps(props, state) {
    console.log("getDerivedStateFromProps Child", props, state);
    return {
      ...state,
      numberChild: props.number * 2,
    };
  }

  render() {
    return (
      <div>
        Child
        {this.state.numberChild}
      </div>
    );
  }

  componentDidMount() {
    console.log("componentDidMount Child");
  }
}
