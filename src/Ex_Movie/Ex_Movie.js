import React, { Component } from "react";
import List from "./List";
import Detail from "./Detail";
import { movieArr } from "./data";

export default class Ex_Movie extends Component {
  state = {
    movieArr: movieArr,
    detail: movieArr[0],
  };

  handleDetail = (movie) => {
    this.setState({
      detail: movie,
    });
  };
  render() {
    return (
      <div>
        <h1>Movie</h1>
        <div className="row">
          <List
            movies={this.state.movieArr}
            handleDetailList={this.handleDetail}
          />
          <Detail detail={this.state.detail} />
        </div>
      </div>
    );
  }
}
