import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { arrItem, handleDetailItem, handleBuyItem } = this.props;
    let { image, name, price } = arrItem;
    return (
      <div className="col-2 my-2">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p class="card-text">Price: {price}</p>
          </div>
          <button
            onClick={() => {
              handleDetailItem(arrItem);
            }}
            className="btn btn-primary"
          >
            Details
          </button>
          <button
            onClick={() => {
              handleBuyItem(arrItem);
            }}
            className="btn btn-success"
          >
            Buy
          </button>
        </div>
      </div>
    );
  }
}
