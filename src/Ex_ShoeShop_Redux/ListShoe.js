import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
import { connect } from "react-redux";

class ListShoe extends Component {
  renderArr = () => {
    return this.props.listShoe.map((item, index) => {
      return (
        <ItemShoe
          // handleDetailItem={this.props.handleDetailList}
          key={index}
          arrItem={item}
          handleBuyItem={this.props.handleBuyList}
        />
      );
    });
  };
  render() {
    return <div className="row col-6">{this.renderArr()}</div>;
  }
}

let mapStateToProps = (state) => {
  return { listShoe: state.shoeReducer.shoeArr };
};
export default connect(mapStateToProps)(ListShoe);
