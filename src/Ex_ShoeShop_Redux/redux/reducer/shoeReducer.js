import { shoeArr } from "../../data";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  DELETE_SHOE,
  VIEW_DETAIL,
} from "../constant/shoeConstant";

let initialState = {
  shoeArr: shoeArr,
  detail: shoeArr[0],
  cart: [],
};

export const shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case VIEW_DETAIL: {
      // payload contains shoe
      state.detail = payload;
      return { ...state };
    }
    case DELETE_SHOE: {
      let cloneCart = state.cart.filter((item) => item.id !== payload);
      state.cart = cloneCart;
      return { ...state };
    }
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id == payload.id);
      if (index == -1) {
        let newShoe = { ...payload, number: 1 };
        cloneCart.push(newShoe);
        state.cart = cloneCart;
        return { ...state };
      } else {
        cloneCart[index].number++;
        state.cart = cloneCart;
        return { ...state };
      }
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id == payload.shoe.id);
      cloneCart[index].number = cloneCart[index].number + payload.option;
      if (cloneCart[index].number == 0) {
        cloneCart.splice(index, 1);
      }
      state.cart = cloneCart;
      return { ...state };
    }
    default:
      return state;
  }
};
