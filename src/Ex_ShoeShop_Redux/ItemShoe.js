import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, VIEW_DETAIL } from "./redux/constant/shoeConstant";
import { viewDetailAction } from "./redux/action/shoeAction";
class ItemShoe extends Component {
  render() {
    let { arrItem, handleDetailItem, handleBuyItem } = this.props;
    let { image, name, price } = arrItem;
    return (
      <div className="col-4 my-2">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p class="card-text">Price: {price}</p>
          </div>
          <button
            onClick={() => {
              handleDetailItem(arrItem);
            }}
            className="btn btn-primary"
          >
            Details
          </button>
          <button
            onClick={() => {
              handleBuyItem(arrItem);
            }}
            className="btn btn-success"
          >
            Buy
          </button>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleDetailItem: (shoe) => {
      dispatch(
        // before: dispatch action (object)
        // now: dispatch function, which return action (object)
        viewDetailAction(shoe)
      );
    },
    handleBuyItem: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
