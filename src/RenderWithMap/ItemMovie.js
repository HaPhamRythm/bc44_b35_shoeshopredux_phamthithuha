import React, { Component } from "react";

export default class ItemMovie extends Component {
  render() {
    // let movie = this.props.data;
    let movie = this.props.itemMovie;
    return (
      // <div className="col col-3">
      //   <div className="card text-left">
      //     <img className="card-img-top" src={movie.hinhAnh} alt="" />
      //     <div className="card-body">
      //       <h4 className="card-title">{movie.tenPhim}</h4>
      //     </div>
      //   </div>
      // </div>
      <div className="col-3">
        <div className="card text-left">
          <img className="card-img-top" src={movie.hinhAnh} alt />
          <div className="card-body">
            <h4 className="card-title">{movie.tenPhim}</h4>
          </div>
        </div>
      </div>
    );
  }
}
