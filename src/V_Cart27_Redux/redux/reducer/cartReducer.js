// import _default from "react-redux/es/components/connect";
// import { shoeArr } from "../../data";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  REMOVE_FROM_CART,
} from "../constant/cartConstant";

let initialState = {
  cart: [],
};

export const cartReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id == payload.id);
      if (index == -1) {
        cloneCart.push(payload);
        state.cart = cloneCart;
        return { ...state };
      } else {
        cloneCart[index].number++;
        state.cart = cloneCart;
        return { ...state };
      }
    }
    case REMOVE_FROM_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id == payload.id);
      cloneCart.splice(index, 1);
      state.cart = cloneCart;
      return { ...state };
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      cloneCart[payload.index].number += payload.inDe;
      state.cart = cloneCart;
      return { ...state };
    }
    default:
      return state;
  }
};
