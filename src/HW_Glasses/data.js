export let glassesArr = [
  { image: "./glasses/glassesImage/v1.png" },
  { image: "./glasses/glassesImage/v2.png" },
  { image: "./glasses/glassesImage/v3.png" },
  { image: "./glasses/glassesImage/v4.png" },
  { image: "./glasses/glassesImage/v5.png" },
  { image: "./glasses/glassesImage/v6.png" },
  { image: "./glasses/glassesImage/v7.png" },
  { image: "./glasses/glassesImage/v8.png" },
  { image: "./glasses/glassesImage/v9.png" },
];

export let background = "./glasses/glassesImage/background.jpg";
