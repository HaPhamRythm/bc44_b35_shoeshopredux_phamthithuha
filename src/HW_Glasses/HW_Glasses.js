import React, { Component } from "react";
import Model from "./Model";
import List from "./List";
import { background, glassesArr } from "./data";

export default class HW_Glasses extends Component {
  state = {
    glassesArr: glassesArr,
    selectedGlasses: glassesArr[0],
  };

  handChange = (glasses) => {
    return this.setState({ selectedGlasses: glasses });
  };
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${background})`,
          height: "100vh",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      >
        <h1 className="bg bg-dark text-white">TRY GLASSES ONLINE APP</h1>
        <div>
          <Model glassModel={this.state.selectedGlasses} />

          {/* <div className="row"> */}
          <List
            list={this.state.glassesArr}
            handleChangeToList={this.handChange}
          />
          {/* </div> */}
        </div>
      </div>
    );
  }
}
